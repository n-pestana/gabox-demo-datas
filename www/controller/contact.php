<?php

$posted = false;
# if posted and ok 
# posted data are automaticaly stored into gen_posted

if(defined('FORM_RESULT_CONTACT') && FORM_RESULT_CONTACT === true) { 
    include(VIEWS_DIR.'/blocks/contact-validation.php');
} else {
    
    # if errors
    if(defined('FORM_RESULT_CONTACT') && FORM_RESULT_CONTACT === false) {  
        $posted = false;
    }

    include(VIEWS_DIR.'/blocks/contact.php');
} 

