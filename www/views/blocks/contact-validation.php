<section id="contact" class="contact">

      <div class="container aos-init aos-animate" data-aos="fade-up">

        <header class="section-header">
          <h2>Contact</h2>
          <p>Contact Us</p>
        </header>

        <div class="row">
             <div class="text-center info-box ">Your message has been sent. Thank you!</div> 
        </div>

        <div class="row text-center p-4">
            <div class="box aos-init aos-animate" data-aos="fade-up" data-aos-delay="400">
              <img src="/views/assets/img/values-2.png" class="img-fluid" alt="">
              <h3>Voluptatem voluptatum alias</h3>
              <p>Repudiandae amet nihil natus in distinctio suscipit id. Doloremque ducimus ea sit non.</p>
            </div>

        </div>

      </div>

    </section>
